## [8.x-1.1] - décembre 2023

### Added

- mise en place de la navigation entre les oeuvres d'une collection _(vue `oeuvres_d_une_collection_navigable`)_
- ajout d'un mode d'affichage `Vue collection` pour une oeuvre
- mise en place d'une classe CSS pour l'affichage d'une oeuvre en fonction de son type
```html
<div class="contextual-region oeuvre_entity oeuvre_entity_peinture">...</div>
```

### Changed

- changement du mode d'affichage pour la vue `oeuvres_d_une_collection`
- mise en place d'un tri sur la vue `oeuvres_d_une_collection`
- ajout d'une pagination avec 5 oeuvres par page pour la vue `oeuvres_d_une_collection`

## [8.x-1.0]

### Added

- Initialisation du projet

# Templates:

## [x.x] - date

### Added

### Changed

### Removed

### Deprecated

### Fixed

### Security
