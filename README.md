# Oeuvres et collections

Module de gestion des oeuvres et des collections pour les sites des musées.

## Ce que fournit le module

### Trois entités

- Oeuvre
- Artiste
- Collection

### Trois vocabulaires

- localisation de l'oeuvre dans le musée
- période de l'oeuvre
- type de l'oeuvre

### Un type de paragraphe pour le champ 'paragraphs' d'une oeuvre

### Des groupes de menus

  - `Contenu > Oeuvres & Collections` permettant l'ajout de contenu pour les entités précédemment créées
  - Structure > Oeuvres & Collections` permettant la configuration des entités précédemment créées

### Deux vues

- une vue à destination de la page d'accueil pour l'affichage la fonctionnalité "un jour une oeuvre" _(via le positionnement d'un bloc restreint pour la page `<front>`)_
- une vue des oeuvres d'une collection à destination des pages de collections
  - via le positionnement d'un bloc restreint pour les pages `/oeuvres_collections/collection_entity/*`
  - dans la zone de contenu
- une vue de navigation dans les oeuvres d'une collection accessible au clic sur le titre de l'oeuvre depuis les pages `/oeuvres_collections/collection_entity/*`

### Un mode d'affichage pour une oeuvre

- le mode d'affichage `Vue collection` est utilisé par la vue `oeuvres_d_une_collection_navigable`

### Une classe CSS en fonction du type de l'oeuvre permettant un affichage spécifique si besoin

```html
<div class="contextual-region oeuvre_entity oeuvre_entity_peinture">
  ...
</div>
```

### NOTE

- Les trois entités et les trois vocabulaires sont traduisibles, les éléments qui finalisent leur configuration sont les fichiers `language.content_settings.*.yml.`
- Le module initialise les permissions afin de voir les contenus publiés _(pour les entités précédemment créées)_ pour les anonymes et les authentifiés
