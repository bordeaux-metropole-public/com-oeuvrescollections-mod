<?php

namespace Drupal\oeuvres_collections;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Oeuvre entity.
 *
 * @see \Drupal\oeuvres_collections\Entity\OeuvreEntity.
 */
class OeuvreEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\oeuvres_collections\Entity\OeuvreEntityInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished oeuvre entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published oeuvre entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit oeuvre entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete oeuvre entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add oeuvre entities');
  }


}
