<?php

namespace Drupal\oeuvres_collections;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Collection entities.
 *
 * @ingroup oeuvres_collections
 */
class CollectionEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = [
      'data' => $this->t('Collection ID'),
      'field' => 'id',
      'specifier' => 'id',
      'sort' => 'desc',
    ];
    $header['name'] = [
      'data' =>  $this->t('Name'),
      'field' => 'name',
      'specifier' => 'name',
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\oeuvres_collections\Entity\CollectionEntity $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.collection_entity.edit_form',
      ['collection_entity' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

  protected function getEntityIds() {
    $header = $this->buildHeader();

    $query = $this->getStorage()->getQuery()
      ->accessCheck(TRUE)
      ->tableSort($header);

    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }
}
