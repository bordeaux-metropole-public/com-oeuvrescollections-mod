<?php

namespace Drupal\oeuvres_collections\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Collection entities.
 *
 * @ingroup oeuvres_collections
 */
class CollectionEntityDeleteForm extends ContentEntityDeleteForm {


}
