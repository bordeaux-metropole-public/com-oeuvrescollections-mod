<?php

namespace Drupal\oeuvres_collections\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Oeuvre entities.
 *
 * @ingroup oeuvres_collections
 */
class OeuvreEntityDeleteForm extends ContentEntityDeleteForm {


}
