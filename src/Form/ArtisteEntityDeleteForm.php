<?php

namespace Drupal\oeuvres_collections\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Artiste entities.
 *
 * @ingroup oeuvres_collections
 */
class ArtisteEntityDeleteForm extends ContentEntityDeleteForm {


}
