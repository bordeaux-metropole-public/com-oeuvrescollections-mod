<?php

namespace Drupal\oeuvres_collections\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Oeuvre revision.
 *
 * @ingroup oeuvres_collections
 */
class OeuvreEntityRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Oeuvre revision.
   *
   * @var \Drupal\oeuvres_collections\Entity\OeuvreEntityInterface
   */
  protected $revision;

  /**
   * The Oeuvre storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $oeuvreEntityStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->oeuvreEntityStorage = $container->get('entity_type.manager')->getStorage('oeuvre_entity');
    $instance->connection = $container->get('database');
    $instance->dateFormatter = $container->get('date.formatter');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'oeuvre_entity_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.oeuvre_entity.version_history', ['oeuvre_entity' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $oeuvre_entity_revision = NULL) {
    $this->revision = $this->oeuvreEntityStorage->loadRevision($oeuvre_entity_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->oeuvreEntityStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Oeuvre: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage(t('Revision from %revision-date of Oeuvre %title has been deleted.', ['%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.oeuvre_entity.canonical',
       ['oeuvre_entity' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {oeuvre_entity_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.oeuvre_entity.version_history',
         ['oeuvre_entity' => $this->revision->id()]
      );
    }
  }

}
