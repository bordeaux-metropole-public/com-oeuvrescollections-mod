<?php

namespace Drupal\oeuvres_collections;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for oeuvre_entity.
 */
class OeuvreEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
