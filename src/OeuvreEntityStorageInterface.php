<?php

namespace Drupal\oeuvres_collections;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\oeuvres_collections\Entity\OeuvreEntityInterface;

/**
 * Defines the storage handler class for Oeuvre entities.
 *
 * This extends the base storage class, adding required special handling for
 * Oeuvre entities.
 *
 * @ingroup oeuvres_collections
 */
interface OeuvreEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Oeuvre revision IDs for a specific Oeuvre.
   *
   * @param \Drupal\oeuvres_collections\Entity\OeuvreEntityInterface $entity
   *   The Oeuvre entity.
   *
   * @return int[]
   *   Oeuvre revision IDs (in ascending order).
   */
  public function revisionIds(OeuvreEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Oeuvre author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Oeuvre revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\oeuvres_collections\Entity\OeuvreEntityInterface $entity
   *   The Oeuvre entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(OeuvreEntityInterface $entity);

  /**
   * Unsets the language for all Oeuvre with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
