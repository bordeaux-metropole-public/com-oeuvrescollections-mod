<?php

namespace Drupal\oeuvres_collections;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\oeuvres_collections\Entity\OeuvreEntityInterface;

/**
 * Defines the storage handler class for Oeuvre entities.
 *
 * This extends the base storage class, adding required special handling for
 * Oeuvre entities.
 *
 * @ingroup oeuvres_collections
 */
class OeuvreEntityStorage extends SqlContentEntityStorage implements OeuvreEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(OeuvreEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {oeuvre_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {oeuvre_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(OeuvreEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {oeuvre_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('oeuvre_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
