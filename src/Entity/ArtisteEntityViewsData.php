<?php

namespace Drupal\oeuvres_collections\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Artiste entities.
 */
class ArtisteEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
