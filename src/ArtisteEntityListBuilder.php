<?php

namespace Drupal\oeuvres_collections;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Artiste entities.
 *
 * @ingroup oeuvres_collections
 */
class ArtisteEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = [
      'data' => $this->t('Artiste ID'),
      'field' => 'id',
      'specifier' => 'id',
      'sort' => 'desc',
    ];
    $header['name'] = [
      'data' =>  $this->t('Name'),
      'field' => 'name',
      'specifier' => 'name',
    ];

    $header['firstname'] = [
      'data' =>  $this->t('Firstname'),
      'field' => 'firstname',
      'specifier' => 'firstname',
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\oeuvres_collections\Entity\ArtisteEntity $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.artiste_entity.edit_form',
      ['artiste_entity' => $entity->id()]
    );
    $row['firstname'] = $entity->getFirstname();
    return $row + parent::buildRow($entity);
  }

  protected function getEntityIds() {
    $header = $this->buildHeader();

    $query = $this->getStorage()->getQuery()
      ->accessCheck(TRUE)
      ->tableSort($header);

    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }
}
