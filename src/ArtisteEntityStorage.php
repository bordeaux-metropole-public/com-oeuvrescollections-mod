<?php

namespace Drupal\oeuvres_collections;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\oeuvres_collections\Entity\ArtisteEntityInterface;

/**
 * Defines the storage handler class for Artiste entities.
 *
 * This extends the base storage class, adding required special handling for
 * Artiste entities.
 *
 * @ingroup oeuvres_collections
 */
class ArtisteEntityStorage extends SqlContentEntityStorage implements ArtisteEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(ArtisteEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {artiste_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {artiste_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(ArtisteEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {artiste_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('artiste_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
