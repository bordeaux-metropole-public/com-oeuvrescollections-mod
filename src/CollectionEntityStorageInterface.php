<?php

namespace Drupal\oeuvres_collections;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\oeuvres_collections\Entity\CollectionEntityInterface;

/**
 * Defines the storage handler class for Collection entities.
 *
 * This extends the base storage class, adding required special handling for
 * Collection entities.
 *
 * @ingroup oeuvres_collections
 */
interface CollectionEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Collection revision IDs for a specific Collection.
   *
   * @param \Drupal\oeuvres_collections\Entity\CollectionEntityInterface $entity
   *   The Collection entity.
   *
   * @return int[]
   *   Collection revision IDs (in ascending order).
   */
  public function revisionIds(CollectionEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Collection author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Collection revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\oeuvres_collections\Entity\CollectionEntityInterface $entity
   *   The Collection entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(CollectionEntityInterface $entity);

  /**
   * Unsets the language for all Collection with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
