<?php

namespace Drupal\oeuvres_collections;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for artiste_entity.
 */
class ArtisteEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
