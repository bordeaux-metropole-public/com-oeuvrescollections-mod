<?php

namespace Drupal\oeuvres_collections;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for collection_entity.
 */
class CollectionEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
