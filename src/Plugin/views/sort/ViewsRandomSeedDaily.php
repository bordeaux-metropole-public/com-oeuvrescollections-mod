<?php

namespace Drupal\oeuvres_collections\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\SortPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\search_api\Plugin\views\query\SearchApiQuery;

/**
 * Handle a random sort with seed.
 *
 * @ViewsSort("views_random_seed_daily")
 */
class ViewsRandomSeedDaily extends SortPluginBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $db_type = \Drupal::database()->driver();
    switch ($db_type) {
      case 'mysql':
      case 'mysqli':
        $formula = 'RAND(CURDATE()+0)';
        break;
      default:
        break;
    }

    if (!empty($formula)) {
      // Use SearchAPI random sorting with seed if the query object is an
      // instance of SearchApiViewsQuery (or a subclass of it). Pass along the
      // seed and the built formula as options for the SearchApiQuery class.
      // See: https://www.drupal.org/node/1197538#comment-10190520
      if ($this->view->query instanceof SearchApiQuery) {
        $this->query->addOrderBy('rand', NULL, NULL, '', [
          'seed' => '',
          'formula' => $formula,
        ]);
      }
      else {
        $this->query->addOrderBy(NULL, $formula, $this->options['order'], '_' . $this->field);
      }
    }
  }
}
