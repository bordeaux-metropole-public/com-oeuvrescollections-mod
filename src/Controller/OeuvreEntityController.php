<?php

namespace Drupal\oeuvres_collections\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\oeuvres_collections\Entity\OeuvreEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OeuvreEntityController.
 *
 *  Returns responses for Oeuvre routes.
 */
class OeuvreEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Oeuvre revision.
   *
   * @param int $oeuvre_entity_revision
   *   The Oeuvre revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($oeuvre_entity_revision) {
    $oeuvre_entity = $this->entityTypeManager()->getStorage('oeuvre_entity')
      ->loadRevision($oeuvre_entity_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('oeuvre_entity');

    return $view_builder->view($oeuvre_entity);
  }

  /**
   * Page title callback for a Oeuvre revision.
   *
   * @param int $oeuvre_entity_revision
   *   The Oeuvre revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($oeuvre_entity_revision) {
    $oeuvre_entity = $this->entityTypeManager()->getStorage('oeuvre_entity')
      ->loadRevision($oeuvre_entity_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $oeuvre_entity->label(),
      '%date' => $this->dateFormatter->format($oeuvre_entity->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Oeuvre.
   *
   * @param \Drupal\oeuvres_collections\Entity\OeuvreEntityInterface $oeuvre_entity
   *   A Oeuvre object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(OeuvreEntityInterface $oeuvre_entity) {
    $account = $this->currentUser();
    $oeuvre_entity_storage = $this->entityTypeManager()->getStorage('oeuvre_entity');

    $langcode = $oeuvre_entity->language()->getId();
    $langname = $oeuvre_entity->language()->getName();
    $languages = $oeuvre_entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $oeuvre_entity->label()]) : $this->t('Revisions for %title', ['%title' => $oeuvre_entity->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all oeuvre revisions") || $account->hasPermission('administer oeuvre entities')));
    $delete_permission = (($account->hasPermission("delete all oeuvre revisions") || $account->hasPermission('administer oeuvre entities')));

    $rows = [];

    $vids = $oeuvre_entity_storage->revisionIds($oeuvre_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\oeuvres_collections\OeuvreEntityInterface $revision */
      $revision = $oeuvre_entity_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $oeuvre_entity->getRevisionId()) {
          $link = Link::fromTextAndUrl($date, new Url('entity.oeuvre_entity.revision', [
            'oeuvre_entity' => $oeuvre_entity->id(),
            'oeuvre_entity_revision' => $vid,
          ]))->toString();
        }
        else {
          $link = $oeuvre_entity->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.oeuvre_entity.translation_revert', [
                'oeuvre_entity' => $oeuvre_entity->id(),
                'oeuvre_entity_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.oeuvre_entity.revision_revert', [
                'oeuvre_entity' => $oeuvre_entity->id(),
                'oeuvre_entity_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.oeuvre_entity.revision_delete', [
                'oeuvre_entity' => $oeuvre_entity->id(),
                'oeuvre_entity_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['oeuvre_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
