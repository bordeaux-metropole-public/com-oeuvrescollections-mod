<?php

namespace Drupal\oeuvres_collections\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\oeuvres_collections\Entity\CollectionEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CollectionEntityController.
 *
 *  Returns responses for Collection routes.
 */
class CollectionEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Collection revision.
   *
   * @param int $collection_entity_revision
   *   The Collection revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($collection_entity_revision) {
    $collection_entity = $this->entityTypeManager()->getStorage('collection_entity')
      ->loadRevision($collection_entity_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('collection_entity');

    return $view_builder->view($collection_entity);
  }

  /**
   * Page title callback for a Collection revision.
   *
   * @param int $collection_entity_revision
   *   The Collection revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($collection_entity_revision) {
    $collection_entity = $this->entityTypeManager()->getStorage('collection_entity')
      ->loadRevision($collection_entity_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $collection_entity->label(),
      '%date' => $this->dateFormatter->format($collection_entity->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Collection.
   *
   * @param \Drupal\oeuvres_collections\Entity\CollectionEntityInterface $collection_entity
   *   A Collection object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(CollectionEntityInterface $collection_entity) {
    $account = $this->currentUser();
    $collection_entity_storage = $this->entityTypeManager()->getStorage('collection_entity');

    $langcode = $collection_entity->language()->getId();
    $langname = $collection_entity->language()->getName();
    $languages = $collection_entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $collection_entity->label()]) : $this->t('Revisions for %title', ['%title' => $collection_entity->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all collection revisions") || $account->hasPermission('administer collection entities')));
    $delete_permission = (($account->hasPermission("delete all collection revisions") || $account->hasPermission('administer collection entities')));

    $rows = [];

    $vids = $collection_entity_storage->revisionIds($collection_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\oeuvres_collections\CollectionEntityInterface $revision */
      $revision = $collection_entity_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $collection_entity->getRevisionId()) {
          $link = Link::fromTextAndUrl($date, new Url('entity.collection_entity.revision', [
            'collection_entity' => $collection_entity->id(),
            'collection_entity_revision' => $vid,
          ]))->toString();
        }
        else {
          $link = $collection_entity->toLink($date)->toString();
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.collection_entity.translation_revert', [
                'collection_entity' => $collection_entity->id(),
                'collection_entity_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.collection_entity.revision_revert', [
                'collection_entity' => $collection_entity->id(),
                'collection_entity_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.collection_entity.revision_delete', [
                'collection_entity' => $collection_entity->id(),
                'collection_entity_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['collection_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
