<?php

namespace Drupal\oeuvres_collections;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Artiste entity.
 *
 * @see \Drupal\oeuvres_collections\Entity\ArtisteEntity.
 */
class ArtisteEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\oeuvres_collections\Entity\ArtisteEntityInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished artiste entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published artiste entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit artiste entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete artiste entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add artiste entities');
  }


}
