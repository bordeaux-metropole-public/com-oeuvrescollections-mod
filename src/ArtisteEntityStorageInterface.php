<?php

namespace Drupal\oeuvres_collections;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\oeuvres_collections\Entity\ArtisteEntityInterface;

/**
 * Defines the storage handler class for Artiste entities.
 *
 * This extends the base storage class, adding required special handling for
 * Artiste entities.
 *
 * @ingroup oeuvres_collections
 */
interface ArtisteEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Artiste revision IDs for a specific Artiste.
   *
   * @param \Drupal\oeuvres_collections\Entity\ArtisteEntityInterface $entity
   *   The Artiste entity.
   *
   * @return int[]
   *   Artiste revision IDs (in ascending order).
   */
  public function revisionIds(ArtisteEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Artiste author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Artiste revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\oeuvres_collections\Entity\ArtisteEntityInterface $entity
   *   The Artiste entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(ArtisteEntityInterface $entity);

  /**
   * Unsets the language for all Artiste with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
