<?php

/**
 * @file
 * Contains artiste_entity.page.inc.
 *
 * Page callback for Artiste entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Artiste templates.
 *
 * Default template: artiste_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_artiste_entity(array &$variables) {
  // Fetch ArtisteEntity Entity Object.
  $artiste_entity = $variables['elements']['#artiste_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
