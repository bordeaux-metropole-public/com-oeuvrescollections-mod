<?php

/**
 * @file
 * Contains collection_entity.page.inc.
 *
 * Page callback for Collection entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Collection templates.
 *
 * Default template: collection_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_collection_entity(array &$variables) {
  // Fetch CollectionEntity Entity Object.
  $collection_entity = $variables['elements']['#collection_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
