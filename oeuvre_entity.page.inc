<?php

/**
 * @file
 * Contains oeuvre_entity.page.inc.
 *
 * Page callback for Oeuvre entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Oeuvre templates.
 *
 * Default template: oeuvre_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_oeuvre_entity(array &$variables) {
  // Fetch OeuvreEntity Entity Object.
  $oeuvre_entity = $variables['elements']['#oeuvre_entity'];

  /* Traitement d'une URL d'accès à l'oeuvre pour navigation entre les oeuvres d'une collection. */
  if (isset($variables['elements']['#url-navigation-oeuvres']) && isset($variables['elements']['name'][0]['#url'])) {
    $variables['elements']['name'][0]['#url'] = $variables['elements']['#url-navigation-oeuvres'];
  }

  /* Traitement d'une info concernant la collection pour laquelle on navigue dans ses oeuvres. */
  if (isset($variables['elements']['#collection_en_cours'])) {
    $variables['content']['#collection_en_cours'] = $variables['elements']['#collection_en_cours'];
  }

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
